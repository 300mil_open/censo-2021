# Censo 2021

This is an example to understand better how works the plugin fro QGIS [quick web viewer](https://gitlab.com/300000-kms/QuickWebViewer).

The repo provides a full QGIS project configured to be exported with Quick Web Viewer consisting in a visualization of the spanish census from 2021.

Instructions:
- download a zipped copy of the repository
- install the plugin QuickWebViewer in your QGIS
- open the file 
- you will see a full project ready to be exported



